
let mysql = require("mysql");
require('dotenv').config();


const configurazioneDB = {
    host: process.env.HOST,
    user: process.env.USR,
    password: process.env.PASSWORD,
    database: process.env.DATABASE,
    port: process.env.PORT
};

let connessioneDB = {

    query: (querySQL, parametri, res) => {

        let connessione = mysql.createConnection(configurazioneDB);

        connessione.query(querySQL, parametri, (errore, datiDatabase) => {

            if (!errore) {

                console.log(datiDatabase);
                return res.json(datiDatabase);
            }
            else {

                console.log(errore);
                return res.json(errore);
            }
        })

    }
}

module.exports = connessioneDB;

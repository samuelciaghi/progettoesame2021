const express = require("express")
const mysql = require('mysql');
const cors = require("cors");
require('dotenv').config();
const app = express();
const connessioneDB = require("./connessioneDB.js")

app.use(cors({ credentials: true, origin: true }));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.get('/', (req, res) => {
    let html = '';
    html += '<a href = "http://localhost:3000/get_studenti"><button >CaricaDati</button></a>'
    res.send(html);
});

//GET
app.get('/get_studenti', (req, res) => {

    let querySQL = 'SELECT s.id, s.nome,s.cognome, s.eta, r.citta, s.stato, TIME_FORMAT(s.entrata, "%k:%i") AS entrata'
        + ', TIME_FORMAT(s.uscita, "%k:%i") AS uscita '
        + 'FROM studenti s join residenza r on s.FKResidenza = r.id order by s.id'

    connessioneDB.query(querySQL, null, res);

});

//GET Id
app.get('/get_studenti/:id', (req, res) => {

    let id_dato = req.params.id;
    let querySQLconID = 'SELECT s.id, s.nome,s.cognome, s.eta, r.citta, s.stato, TIME_FORMAT(s.entrata, "%k:%i") AS entrata'
        + ', TIME_FORMAT(s.uscita, "%k:%i") AS uscita '
        + 'FROM studenti s join residenza r on s.FKResidenza = r.id  WHERE s.id = ? order by s.id  '

    connessioneDB.query(querySQLconID, id_dato, res);

});

//GET Nome
app.get('/get_studente_nome/:nome', (req, res) => {

    let nomeStudente = req.params.nome;
    let querySQLStudente = 'SELECT s.id, s.nome,s.cognome, s.eta, r.citta, s.stato, TIME_FORMAT(s.entrata, "%k:%i") AS entrata'
        + ', TIME_FORMAT(s.uscita, "%k:%i") AS uscita '
        + 'FROM studenti s join residenza r on s.FKResidenza = r.id  WHERE s.nome = ?order by s.id  '

    connessioneDB.query(querySQLStudente, nomeStudente, res);
});

//POST

app.post('/aggiungi_studente', (req, res) => {

    let studente = {
        id: req.body.id,
        nome: req.body.nome,
        cognome: req.body.cognome,
        eta: req.body.eta,
        FKResidenza: req.body.FKResidenza,
        FKVoti: req.body.FKVoti,
        stato: req.body.stato,
        entrata: req.body.entrata,
        uscita: req.body.uscita
    }

    let querySQL_POST = "INSERT INTO studenti SET ?";

    connessioneDB.query(querySQL_POST, studente, res);

});




//PUT

app.put('/modifica_studente/:id', (req, res) => {

    let id = req.params.id;
    let id_modificato = req.body.id;
    let nome = req.body.nome;
    let cognome = req.body.cognome;
    let eta = req.body.eta;
    let FKResidenza = req.body.FKResidenza;
    let FKVoti = req.body.FKVoti || null;
    let stato = req.body.stato;
    let entrata = req.body.entrata;
    let uscita = req.body.uscita || "";

    let querySQL_PUT = " UPDATE studenti SET id = "
        + id_modificato + ",nome = '"
        + nome + "',cognome = '"
        + cognome + "',eta = "
        + eta + ",FKResidenza = '"
        + FKResidenza + "',FKVoti = "
        + FKVoti + ",stato  = "
        + stato + " , entrata = '"
        + entrata + "', uscita = '"
        + uscita + "' WHERE id = "
        + id;

    connessioneDB.query(querySQL_PUT, null, res);
});

//PUT Presenza
app.put('/modifica_presenza/:id', (req, res) => {

    let id = req.params.id;
    let stato = req.body.stato;

    let querySQL_Presenza = " UPDATE studenti SET stato  = "
        + stato + "  WHERE id = "
        + id;

    connessioneDB.query(querySQL_Presenza, null, res);

});

//PUT Entrate
app.put('/modifica_entrata/:id', (req, res) => {

    let id = req.params.id;
    let entrata = req.body.entrata;

    let querySQL_Entrata = " UPDATE studenti SET entrata  = '"
        + entrata + "'  WHERE id = "
        + id;

    connessioneDB.query(querySQL_Entrata, null, res);

});

//PUT Uscite
app.put('/modifica_uscita/:id', (req, res) => {

    let id = req.params.id;
    let uscita = req.body.uscita;

    let querySQL_Uscita = " UPDATE studenti SET uscita  = '"
        + uscita + "'  WHERE id = "
        + id;

    connessioneDB.query(querySQL_Uscita, null, res);
});



//DELETE

app.delete('/elimina_studente/:id', (req, res) => {

    let querySQL_DELETE = 'DELETE FROM studenti WHERE id = ?'
    let is_studente = req.params.id;

    connessioneDB.query(querySQL_DELETE, is_studente, res);

});


app.listen(3000, () =>
    console.log('PORTA 3000'),
);
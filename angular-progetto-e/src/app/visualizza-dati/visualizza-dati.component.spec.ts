import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizzaDatiComponent } from './visualizza-dati.component';

describe('VisualizzaDatiComponent', () => {
  let component: VisualizzaDatiComponent;
  let fixture: ComponentFixture<VisualizzaDatiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisualizzaDatiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizzaDatiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

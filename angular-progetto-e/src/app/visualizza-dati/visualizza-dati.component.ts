import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { GetDatiService } from '../servizio/get-dati.service';
import { ClassGetter } from '@angular/compiler/src/output/output_ast';
import { Studente } from '../servizio/studente';
import { Observable } from 'rxjs';
import { CercaStudentePipe } from '../servizio/cerca-studente.pipe';

@Component({
  selector: 'app-visualizza-dati',
  templateUrl: './visualizza-dati.component.html',
  styleUrls: ['./visualizza-dati.component.css']
})
export class VisualizzaDatiComponent implements OnInit {

  datiRicevuti: Studente[] = [];
  datoFiltrato: any = [];
  indexEntrata: number = 1;
  indexUscita: number = 1;

  id = null
  nome = ''
  cognome = ''
  eta = 18
  fkresidenza = 'Rovereto'
  fkvoti = 0
  stato: any
  entrata = ''
  uscita = ''


  id_selezionato: number = undefined as unknown as number;
  nomeInserito = '';
  statoPresenza: string = '';

  constructor(private datiService: GetDatiService) { }



  ngOnInit(): void {
    this.visualizzaDati();

  }


  //GET 
  visualizzaDati() {
    this.datiService.getDati().subscribe((dati : Studente[])  => {
      this.datiRicevuti = dati;
      //Assegno  i dati ricevuti alla variabile service per passare i dati fra i componenti
      this.datiService.datiRicevutiService = this.datiRicevuti;
      console.log(this.datiRicevuti);
    })

  }

  //GET con ID
  filtroPerId() {
    console.log(this.id_selezionato)
    this.datiService.getDatoPerId(this.id_selezionato).subscribe(dato => {
      this.datiRicevuti = dato;
      console.log(this.datiRicevuti)
    })
  }

  //GET nome
  cercaStudente() {
    /*  
     this.datiService.getDatoNome(this.nomeInserito).subscribe(dato => {
       this.datiRicevuti = dato;
       console.log(this.datiRicevuti)
     }) */
    console.log("Studente cercato: " + this.nomeInserito);
  }

  //PUT
  modificaStudente(indice: any) {

    let studenteModificato = {}

    this.datiRicevuti.map((studente) => {
      this.stato = 1 - studente.stato;
      studenteModificato = {
        id: studente.id,
        nome: studente.nome,
        cognome: studente.cognome,
        eta: studente.eta,
        FKResidenza: studente.FKResidenza, //Da cambiare in NodeJs r.citta in s.FKResidenza perchè funzioni
        FKVoti: studente.FKVoti,
        stato: studente.stato,
        entrata: studente.entrata,
        uscita: studente.uscita
      }
      return studenteModificato
    });

    this.datiService.putStudente(indice, { ...studenteModificato, stato: this.stato }).subscribe(stud => {
      console.log('Studente Modificato: ' + stud + "---" + studenteModificato);
    })
  }


  //PUT Presenza
  modificaPresenza(indice: any) {
  
    //Seleziona lo studente con l'id uguale all'indice passato nel HTML
    let [statoStudente] = this.datiRicevuti.filter(el => el.id == indice);

    this.datiService.putPresenza(indice, { stato: 1 - statoStudente.stato })
      .subscribe(() => {
        this.visualizzaDati();
      });
  }

  //PUT Entrata 
  modificaEntrata(indice: any) {
    this.datiService.putEntrata(indice, { entrata: this.entrata }).subscribe(() => { this.visualizzaDati()});
  }

  //PUT Uscita
  modificaUscita(indice: any) {
    this.datiService.putUscita(indice, { uscita: this.uscita }).subscribe(() => { this.visualizzaDati()});
  }

  //DELETE
  eliminaStudente(indice: any, nome: any, cognome: any) {
    this.datiService.deleteStudente(indice).subscribe(dato => {
      console.log("Eliminato Studente: " + nome + " " + cognome + ", con ID: " + indice);
      //Aggiorna il DOM ad ogni DELETE request
      this.visualizzaDati();
    })
  }

  clickTest(indice: any) {console.log('click indice: ' + indice)}


}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AggiungiStudenteComponent } from './aggiungi-studente/aggiungi-studente.component';
import { VisualizzaDatiComponent } from './visualizza-dati/visualizza-dati.component';
import { VotiComponent } from './voti/voti.component';
import {MatButtonToggleModule} from '@angular/material/button-toggle';


const routes: Routes = [
  {path:'', component: VisualizzaDatiComponent},
  {path:'aggiungi_studente', component: AggiungiStudenteComponent},
  {path:'voti', component: VotiComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

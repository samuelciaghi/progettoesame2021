import { TestBed } from '@angular/core/testing';

import { GetDatiService } from './get-dati.service';

describe('GetDatiService', () => {
  let service: GetDatiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GetDatiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

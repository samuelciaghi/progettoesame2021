export interface Studente {

    id: number,
    nome: String,
    cognome: String,
    eta: number,
    FKResidenza: any,
    FKVoti?:null,
    stato: number,
    entrata?: string,
    uscita?: String

    
}

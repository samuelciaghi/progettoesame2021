import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cercaStudente'
})
export class CercaStudentePipe implements PipeTransform {

 
  transform(value: any, args?: any): any {
    if(!args)
     return value;
    return value.filter(
      (      studente: { nome: string; }) => studente.nome.toLowerCase().indexOf(args.toLowerCase()) > -1
   );
  }

}

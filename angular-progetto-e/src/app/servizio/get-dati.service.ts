import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Studente } from './studente';



@Injectable({
  providedIn: 'root'
})
export class GetDatiService {

  datiRicevutiService: Studente[] = [];

   headers = new HttpHeaders({ 'Content-Type': 'application/json' });

   //datiConnessione = { headers: this.headers, responseType: 'text' };


  constructor(private miohttp: HttpClient) { }

  getDati(): Observable<Studente[]> {
    return this.miohttp.get<Studente[]>("http://localhost:3000/get_studenti");
  }

  getDatoPerId(id: number):Observable<Studente[]> {

    if (id > 0 || typeof id === undefined) {
      return this.miohttp.get<Studente[]>("http://localhost:3000/get_studenti/" + id);
    } else {
      return this.miohttp.get<Studente[]>("http://localhost:3000/get_studenti");
    }
  }

  getDatoNome(nome: String):Observable<Studente[]> {
    if (nome !== '') {
      return this.miohttp.get<Studente[]>("http://localhost:3000/get_studente_nome/" + nome)
    }
    else {
      return this.miohttp.get<Studente[]>("http://localhost:3000/get_studenti");
    }
  }

  postStudente(studente: Studente) {
    return this.miohttp.post("http://localhost:3000/aggiungi_studente", studente, { headers: this.headers, responseType: 'text' })
  }

  putStudente(id: String, dati: any) {
    return this.miohttp.put('http://localhost:3000/modifica_studente/' + id, dati, { headers: this.headers, responseType: 'text' })
  }

  putPresenza(id: String, dato: any) {
    return this.miohttp.put('http://localhost:3000/modifica_presenza/' + id, dato, { headers: this.headers, responseType: 'text' })
  }

  putEntrata(id:String, entrata:any){
    return this.miohttp.put('http://localhost:3000/modifica_entrata/' + id, entrata, { headers: this.headers, responseType: 'text' })
  }

  putUscita(id:String, uscita:any){
    return this.miohttp.put('http://localhost:3000/modifica_uscita/' + id, uscita, { headers: this.headers, responseType: 'text' })
  }

  deleteStudente(id: number) {
    return this.miohttp.delete('http://localhost:3000/elimina_studente/' + id, { headers: this.headers, responseType: 'text' })
  }







}

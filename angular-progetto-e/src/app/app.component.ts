import { Component } from '@angular/core';
import { GetDatiService } from './servizio/get-dati.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-progetto-e';

  constructor(private router: Router) {

  }

  ngOnInit() {
    //Ogni refresh della pagina reindirizza alla pagina root (Studente)
    this.router.navigate(['/'])
  }


}

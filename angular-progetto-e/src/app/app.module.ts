import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { VisualizzaDatiComponent } from './visualizza-dati/visualizza-dati.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AggiungiStudenteComponent } from './aggiungi-studente/aggiungi-studente.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { VotiComponent } from './voti/voti.component';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { NgSelectModule } from '@ng-select/ng-select';
import { CercaStudentePipe } from './servizio/cerca-studente.pipe';


@NgModule({
  declarations: [
    AppComponent,
    VisualizzaDatiComponent,
    AggiungiStudenteComponent,
    VotiComponent,
    CercaStudentePipe,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatInputModule,
    MatToolbarModule,
    MatCardModule,
    MatIconModule,
    MatSelectModule,
    MatButtonToggleModule,
    NgSelectModule,


  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

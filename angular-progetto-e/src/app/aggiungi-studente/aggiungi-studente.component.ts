import { Component, OnInit } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { GetDatiService } from '../servizio/get-dati.service';
import { Studente } from '../servizio/studente';
import { MatSelectModule } from '@angular/material/select';



@Component({
  selector: 'app-aggiungi-studente',
  templateUrl: './aggiungi-studente.component.html',
  styleUrls: ['./aggiungi-studente.component.css']
})
export class AggiungiStudenteComponent implements OnInit {

  aggiunto: boolean = false;
  datiCaricati: boolean = false;
  idCalcolato = 0;

  id = null;
  nome = '';
  cognome = '';
  eta = 18;
  FKResidenza = undefined;
  FKVoti = null;
  stato = 0;
 

  constructor(private miohttp: GetDatiService) { }

  ngOnInit(): void {
    this.aggiunto = false;

    //Disabilita il btn per 2 sec in attesa dei datidall'App.component.ts
    setTimeout(() => { this.datiCaricati = true, console.log("TEST 2", this.miohttp.datiRicevutiService); }, 100);

  }


  aggiungiStudente() {

    this.calcolaIdStudente();
    this.aggiunto = false;

    const nuovoStudente: Studente = {
      id: this.idCalcolato,
      nome: this.nome,
      cognome: this.cognome,
      eta: this.eta,
      FKResidenza: this.FKResidenza,
      FKVoti: this.FKVoti,
      stato: this.stato,
    }

    this.miohttp.postStudente(nuovoStudente).subscribe((e) =>{console.log(e); this.aggiunto = true});

    console.log("Aggiunto Studente: " + this.nome + " " + this.cognome + " FKResidenza: " + this.FKResidenza);

  }

  calcolaIdStudente() {
    //Prendo l'ultimo elemento dell'array studenti
    let [ultimoStudente] = this.miohttp.datiRicevutiService.slice(-1);
    this.idCalcolato = ultimoStudente.id + 1;

  }


}

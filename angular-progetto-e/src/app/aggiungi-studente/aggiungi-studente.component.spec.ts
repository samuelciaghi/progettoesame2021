import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AggiungiStudenteComponent } from './aggiungi-studente.component';

describe('AggiungiStudenteComponent', () => {
  let component: AggiungiStudenteComponent;
  let fixture: ComponentFixture<AggiungiStudenteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AggiungiStudenteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AggiungiStudenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

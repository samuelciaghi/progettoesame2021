-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: progettoe
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `insegnanti`
--

DROP TABLE IF EXISTS `insegnanti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `insegnanti` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nome` varchar(15) NOT NULL,
  `cognome` varchar(15) NOT NULL,
  `materia` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `insegnanti`
--

LOCK TABLES `insegnanti` WRITE;
/*!40000 ALTER TABLE `insegnanti` DISABLE KEYS */;
/*!40000 ALTER TABLE `insegnanti` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `residenza`
--

DROP TABLE IF EXISTS `residenza`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `residenza` (
  `id` int NOT NULL AUTO_INCREMENT,
  `citta` varchar(15) DEFAULT NULL,
  `via` varchar(15) DEFAULT NULL,
  `cap` int DEFAULT NULL,
  `provincia` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `residenza`
--

LOCK TABLES `residenza` WRITE;
/*!40000 ALTER TABLE `residenza` DISABLE KEYS */;
INSERT INTO `residenza` VALUES (1,'Rovereto','Via Monti',38068,'Trento'),(2,'Mori','Via Dante',38065,'Trento'),(3,'Riva Del Garda','Via Venezia',38066,'Trento');
/*!40000 ALTER TABLE `residenza` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `studenti`
--

DROP TABLE IF EXISTS `studenti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `studenti` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nome` varchar(25) NOT NULL,
  `cognome` varchar(25) NOT NULL,
  `eta` int NOT NULL,
  `FKResidenza` int DEFAULT NULL,
  `FKVoti` int DEFAULT NULL,
  `stato` tinyint(1) NOT NULL,
  `entrata` varchar(10) DEFAULT NULL,
  `uscita` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKStudenteResidenza` (`FKResidenza`),
  KEY `FKStudenteVoti` (`FKVoti`),
  CONSTRAINT `FKStudenteResidenza` FOREIGN KEY (`FKResidenza`) REFERENCES `residenza` (`id`),
  CONSTRAINT `FKStudenteVoti` FOREIGN KEY (`FKVoti`) REFERENCES `voti` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `studenti`
--

LOCK TABLES `studenti` WRITE;
/*!40000 ALTER TABLE `studenti` DISABLE KEYS */;
INSERT INTO `studenti` VALUES (1,'Samuel','Ciaghi',18,2,NULL,1,'8:35','0'),(2,'John','Kong',28,2,NULL,0,'0','0'),(3,'Don','Creek',45,1,NULL,1,'0','0'),(4,'Marshall','Teach',17,1,NULL,0,'9:15',NULL),(5,'Joel','Green',21,1,NULL,1,NULL,'12:25'),(6,'Mark','Web',20,2,NULL,0,'','0'),(7,'Luke','Grey',23,3,NULL,0,'11:25',NULL),(8,'George','Harris',20,3,NULL,1,'0',NULL),(10,'Joel','Torres',19,1,NULL,0,'08:35:00','12:55:00'),(11,'Dred','Church',31,3,NULL,1,NULL,'09:35:00'),(12,'Emma','Brown',22,3,NULL,1,'9:00','');
/*!40000 ALTER TABLE `studenti` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabella_prova`
--

DROP TABLE IF EXISTS `tabella_prova`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tabella_prova` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nome` varchar(20) NOT NULL,
  `cognome` varchar(20) NOT NULL,
  `eta` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabella_prova`
--

LOCK TABLES `tabella_prova` WRITE;
/*!40000 ALTER TABLE `tabella_prova` DISABLE KEYS */;
INSERT INTO `tabella_prova` VALUES (1,'Nome1','Cognome1',20),(2,'nome3','cognome3',54);
/*!40000 ALTER TABLE `tabella_prova` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `voti`
--

DROP TABLE IF EXISTS `voti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `voti` (
  `id` int NOT NULL AUTO_INCREMENT,
  `materia` varchar(15) NOT NULL,
  `voto` int NOT NULL,
  `dataInserimento` date DEFAULT NULL,
  `FKInsegnante` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKVotiInsegnanti` (`FKInsegnante`),
  CONSTRAINT `FKVotiInsegnanti` FOREIGN KEY (`FKInsegnante`) REFERENCES `insegnanti` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `voti`
--

LOCK TABLES `voti` WRITE;
/*!40000 ALTER TABLE `voti` DISABLE KEYS */;
/*!40000 ALTER TABLE `voti` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-01-07 19:07:04
